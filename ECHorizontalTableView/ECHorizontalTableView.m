//
//  ECHorizontalTableView.m
//  ECHorizontalTableView
//
//  Created by SU BO-YU on 2013/11/9.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ECHorizontalTableView.h"

@implementation ECHorizontalTableView

//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    if (self = [super initWithCoder:aDecoder])
//    {
//        self.frame = [self verticalRectFromFrame:self.frame];
//    }
//    
//    return self;
//}
//
//- (id)initWithFrame:(CGRect)frame
//{
//    if (self = [super initWithFrame:frame])
//    {
//    }
//    
//    return self;
//}
//
//- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
//{
//    if (self = [super initWithFrame:frame style:style])
//    {
//    }
//    
//    return self;
//}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style rowWidth:(CGFloat)width
{
    //NSLog(@"%f,%f,%f,%f",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height);
    CGRect verticalRect = [self verticalRectFromFrame:frame];
    if (self = [super initWithFrame:verticalRect style:style])
    {
        //NSLog(@"%f,%f,%f,%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        [self.layer setAnchorPoint:CGPointMake(0.0, 0.0)];
        self.frame = CGRectMake(CGRectGetMinX(self.frame) - CGRectGetWidth(self.frame) * 0.5f,
                                CGRectGetMinY(self.frame) - CGRectGetHeight(self.frame) * 0.5f,
                                CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
        //NSLog(@"%f,%f,%f,%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
        self.transform = CGAffineTransformMakeRotation(M_PI / -2);
        self.showsVerticalScrollIndicator = NO;
        self.rowHeight = width;
        //NSLog(@"%f,%f,%f,%f",self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
    }
    
    return self;
}

- (CGRect)verticalRectFromFrame:(CGRect)frame
{
    return CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame) + CGRectGetHeight(frame), CGRectGetHeight(frame), CGRectGetWidth(frame));
}

@end
