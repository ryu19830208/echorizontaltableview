//
//  ECHorizontalTableView.h
//  ECHorizontalTableView
//
//  Created by SU BO-YU on 2013/11/9.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ECHorizontalTableView : UITableView
- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style rowWidth:(CGFloat)width;
@end
