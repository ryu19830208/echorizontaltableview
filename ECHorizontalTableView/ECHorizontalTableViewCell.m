//
//  ECHorizontalTableViewCell.m
//  ECHorizontalTableView
//
//  Created by SU BO-YU on 2013/11/9.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ECHorizontalTableViewCell.h"

@implementation ECHorizontalTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.transform = CGAffineTransformMakeRotation(M_PI / 2);
    }
    
    return self;
}

@end
