//
//  ViewController.m
//  Demo
//
//  Created by SU BO-YU on 2013/11/9.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ViewController.h"
#import "ECHorizontalTableView.h"
#import "TestCell.h"


@interface ViewController ()
@property (strong, nonatomic) NSMutableArray *datas;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor orangeColor];
    
    NSUInteger count = 20;
    self.datas = [NSMutableArray arrayWithCapacity:count];
    for (int i = 1; i <= count; i++)
    {
        [self.datas addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    ECHorizontalTableView *table = [[ECHorizontalTableView alloc] initWithFrame:CGRectMake(0, 50, 320, 100) style:UITableViewStylePlain rowWidth:100];
    table.dataSource = self;
    table.delegate = self;
    [self.view addSubview:table];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    cell.textLabel.text = [self.datas objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectLabel.text = [self.datas objectAtIndex:indexPath.row];
}

@end
