//
//  TestCell.m
//  Demo
//
//  Created by SU BO-YU on 2013/11/9.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "TestCell.h"

@implementation TestCell
@synthesize label1 = _label1;
@synthesize label2 = _label2;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.layer.borderWidth = 0.5f;
        self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        self.label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 24)];
        self.label1.textColor = [UIColor blackColor];
        self.label1.textAlignment = NSTextAlignmentCenter;
        self.label1.text = @"左上";
        [self addSubview:self.label1];
        
        
        self.label2 = [[UILabel alloc] initWithFrame:CGRectMake(60, 76, 40, 24)];
        self.label2.textColor = [UIColor blackColor];
        self.label2.textAlignment = NSTextAlignmentCenter;
        self.label2.text = @"右下";
        [self addSubview:self.label2];
    }
    
    return self;
}


@end
