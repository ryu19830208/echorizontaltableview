//
//  TestCell.h
//  Demo
//
//  Created by SU BO-YU on 2013/11/9.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECHorizontalTableViewCell.h"

@interface TestCell : ECHorizontalTableViewCell
@property (strong, nonatomic) UILabel *label1;
@property (strong, nonatomic) UILabel *label2;
@end
